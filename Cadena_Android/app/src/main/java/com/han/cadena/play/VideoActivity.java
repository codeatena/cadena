package com.han.cadena.play;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

public class VideoActivity extends Activity {

    public static String videoPath;
    HTML5WebView mWebView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mWebView = new HTML5WebView(this);
        mWebView.loadUrl(videoPath);

        Log.e("video path: ", videoPath);
        setContentView(mWebView.getLayout());
    }

    @Override
    public void onStop() {
        mWebView.stopLoading();

        if (mWebView.inCustomView()) {
            mWebView.hideCustomView();
        }
        if (mWebView.canGoBack()) {
            mWebView.goBack();
        }

        mWebView.loadUrl("about:blank");
        super.onStop();
    }
}