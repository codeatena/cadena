package com.han.cadena.model;

/**
 * Created by COREI3 on 2/11/2015.
 */
public class Mas {

    public String title;
    public String desc;
    public String url;

    public int imageResouceID;

    public Mas(String _title, String _desc, String _url, int _imageResourceID) {
        title = _title;
        desc = _desc;
        url = _url;
        imageResouceID = _imageResourceID;
    }
}
