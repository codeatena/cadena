package com.han.cadena.listItem;

/**
 * Created by COREI3 on 2/13/2015.
 */
public class AudioItem {

    public String audioPath;
    public String text;

    public AudioItem(String _audioPath, String _text) {
        audioPath = _audioPath;
        text = _text;
    }
}
