package com.han.cadena;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;

import com.han.cadena.detail.EnvivoDetailFragment;
import com.han.cadena.detail.MasDetailFragment;
import com.han.cadena.detail.NoticaDetailFragment;
import com.han.cadena.model.AudioStream;
import com.han.cadena.model.Envivo;
import com.han.cadena.model.Mas;
import com.han.cadena.model.Notica;
import com.han.cadena.pager.AudioPageFragment;
import com.han.cadena.tabs.EnvivoFragment;
import com.han.cadena.tabs.MasFragment;
import com.han.cadena.tabs.NoticasFragment;

import java.util.ArrayList;

import wseemann.media.FFmpegMediaPlayer;

public class TabbedFragment extends Fragment implements OnTabChangeListener {

    public static final String CADENA_URL = "http://www.cadena3.com/app/radio.asp?programacion=CADENA%203";
    public static final String CORDOBA_URL = "http://www.cadena3.com/app/radio.asp?programacion=100.5%20FM%20CORDOBA";
    public static final String RADIO_URL = "http://www.cadena3.com/app/radio.asp?programacion=RADIO%20POPULAR%2092.3";
    public static final String EXTRA_URL = "http://www.cadena3.com/app/radio.asp?programacion=LV3%20%E2%80%93%20AM%2070";

    public static final String CADENA_AUDIO_URL = "http://cadena3.activecds.telecomcdn.com.ar/cadena3.mp3";
    public static final String CORDOBA_AUDIO_URL = "http://lv3canal.activecds.telecomcdn.com.ar/lv3canal1.mp3";
    public static final String RADIO_AUDIO_URL = "http://lv3canal.activecds.telecomcdn.com.ar/lv3canal2.mp3";
    public static final String EXTRA__AUDIO_URL = "http://cadena3.activecds.telecomcdn.com.ar/am700.mp3";

    public ArrayList<Envivo> arrEnvivos;

    TabHost mTabHost;

    public static TabbedFragment instance;
    public AudioPageFragment audioPageFragment = null;

    NoticasFragment noticasFragment = new NoticasFragment();
    EnvivoFragment envivoFragment = new EnvivoFragment();
    MasFragment masFragment = new MasFragment();

    public boolean isPlayNotica = false;

    NoticaDetailFragment noticaDetailFragment = new NoticaDetailFragment();
    EnvivoDetailFragment envivoDetailFragment = new EnvivoDetailFragment();
    MasDetailFragment masDetailFragment = new MasDetailFragment();

    final String TABS_NOTICAS = "noticas";
    final String TABS_ENVIVO = "envivo";
    final String TABS_MAS = "mas";

    View rootView;

    FFmpegMediaPlayer noticaPlayer = new FFmpegMediaPlayer();

    public static MediaPlayer playerCadena = new MediaPlayer();
    public static MediaPlayer playerCordova = new MediaPlayer();
    public static MediaPlayer playerRadio = new MediaPlayer();
    public static MediaPlayer playerExtra = new MediaPlayer();

    public static FFmpegMediaPlayer mp;
    AudioStream audioStream;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_tabbed, container, false);
        mTabHost = (TabHost) rootView.findViewById(android.R.id.tabhost);

        initValue();
        setupTabs();
        initEvent();

        instance = this;

        return rootView;
    }

    public static TabbedFragment getInstance() {
        return instance;
    }

    private void initValue() {
        arrEnvivos = new ArrayList<Envivo>();
        arrEnvivos.add(new Envivo("Cadena 3", "La radio AM líder del pais", CADENA_URL, CADENA_AUDIO_URL, R.drawable.cadena));
        arrEnvivos.add(new Envivo("FM Córdoba", "Toda la Música", CORDOBA_URL, CORDOBA_AUDIO_URL, R.drawable.fmcordoba));
        arrEnvivos.add(new Envivo("Radio Popular", "La radio de la gente", RADIO_URL, RADIO_AUDIO_URL, R.drawable.popular));
        arrEnvivos.add(new Envivo("Cadena 3 Extra", "Transmisión Duplex de la Emisora", EXTRA_URL, EXTRA__AUDIO_URL, R.drawable.cadena_extra));

//        AudioWife.getInstance()
//                .init(getActivity(), Uri.parse(CADENA_AUDIO_URL));
//        AudioWife.getInstance().play();
    }

    private void initEvent() {

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setRetainInstance(true);

//        initMedia(playerCadena, CADENA_AUDIO_URL);
//        initMedia(playerCordova, CORDOBA_AUDIO_URL);
//        initMedia(playerRadio, RADIO_AUDIO_URL);
//        initMedia(playerExtra, EXTRA__AUDIO_URL);

//        mp = new FFmpegMediaPlayer();

        mTabHost.setOnTabChangedListener(this);
    }

    public void playEnvivoAudio(String url) {
        try {
            mp = new FFmpegMediaPlayer();
            mp.setDataSource(url);

            mp.setOnPreparedListener(new FFmpegMediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(FFmpegMediaPlayer mp) {
                    mp.start();
                    EnvivoDetailFragment envivoDetailFragment = EnvivoDetailFragment.getInstance();
                    if (envivoDetailFragment != null) {
//                    mp.get
//                    if (url.equals(envivoDetailFragment.audioUrl)) {
                        envivoDetailFragment.startPlay();
//                    }
                    }
                }
            });

            mp.prepareAsync();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void stopEnvivoAudio() {
        if (mp != null) {
            mp.stop();
            mp.reset();
            mp.release();
            mp = null;
        }
    }

    private void setupTabs() {

        FragmentManager fm = getFragmentManager();

        fm.beginTransaction()
                .replace(R.id.tab1, noticasFragment)
                .commit();
        fm.beginTransaction()
                .replace(R.id.tab2, envivoFragment)
                .commit();
        fm.beginTransaction()
                .replace(R.id.tab3, masFragment)
                .commit();

        mTabHost.setup(); // you must call this before adding your tabs!
        mTabHost.addTab(newTab(TABS_NOTICAS, R.string.noticas, R.id.tab1));
        mTabHost.addTab(newTab(TABS_ENVIVO, R.string.envivo, R.id.tab2));
        mTabHost.addTab(newTab(TABS_MAS, R.string.mas, R.id.tab3));

        mTabHost.setCurrentTab(0);
    }

    private TabSpec newTab(String tag, int labelID, int tabContentId) {

        TabSpec tabSpec = mTabHost.newTabSpec(tag);
        tabSpec.setIndicator(createTabView(this.getActivity(), tag));
        tabSpec.setContent(tabContentId);

        return tabSpec;
    }

    private View createTabView(Context parent, final String text) {
        View view = LayoutInflater.from(parent).inflate(R.layout.tabs_bg, null);
        TextView tv = (TextView) view.findViewById(R.id.tabsText);
        ImageView ti = (ImageView) view.findViewById(R.id.tab_icon_image);

        if (text.equals(TABS_NOTICAS)) {
            ti.setImageResource(R.drawable.tab_noticas);
            tv.setText(R.string.noticas);
        }
        if (text.equals(TABS_ENVIVO)) {
            ti.setImageResource(R.drawable.tab_envivo);
            tv.setText(R.string.envivo);
        }
        if (text.equals(TABS_MAS)) {
            ti.setImageResource(R.drawable.tab_mas);
            tv.setText(R.string.mas);
        }

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    private void initMedia(MediaPlayer mediaPlayer, final String url) {
        try {
            if (mediaPlayer != null) {
                mediaPlayer.stop();
                mediaPlayer.reset();
            }

//            mediaPlayer = new MediaPlayer();

//            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setDataSource(url);
            mediaPlayer.setVolume(0, 0);
            mediaPlayer.prepareAsync();

            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(final MediaPlayer mp) {
                    Log.e("audio state: ", url + ": start");
                    Thread t = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            mp.start();
                            AudioStream.pause(url);
                            EnvivoDetailFragment envivoDetailFragment = EnvivoDetailFragment.getInstance();
                            if (envivoDetailFragment != null) {
                                if (url.equals(envivoDetailFragment.audioUrl)) {
                                    envivoDetailFragment.startPlay();
                                }
                            }
                        }
                    });
                    t.start();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTabChanged(String tabId) {
        if (TABS_NOTICAS.equals(tabId)) {
            updateTab(tabId, R.id.tab1);
            return;
        }
        if (TABS_ENVIVO.equals(tabId)) {
            updateTab(tabId, R.id.tab2);
            return;
        }
        if (TABS_MAS.equals(tabId)) {
            updateTab(tabId, R.id.tab3);
            return;
        }
    }

    private void updateTab(String tabId, int placeholder) {
//        FragmentManager fm = getFragmentManager();
//        if (tabId.equals(TABS_NOTICAS)) {
//            fm.beginTransaction()
//                    .replace(placeholder, navNoticasFragment, tabId)
//                    .commit();
//        }
//        if (tabId.equals(TABS_ENVIVO)) {
//            fm.beginTransaction()
//                    .replace(placeholder, navEnvivoFragment, tabId)
//                    .commit();
//        }
//        if (tabId.equals(TABS_MAS)) {
//            fm.beginTransaction()
//                    .replace(placeholder, navMasFragment, tabId)
//                    .commit();
//        }
    }

//    public void showNoticas() {
//        FragmentManager fm = getFragmentManager();
//
//        fm.beginTransaction()
//                .replace(R.id.tab1, noticasFragment)
//                .commit();
//    }
//
//    public void showEnvivo() {
//        FragmentManager fm = getFragmentManager();
//
//        fm.beginTransaction()
//                .replace(R.id.tab2, envivoFragment)
//    }
//
//    public void showMas() {
//        FragmentManager fm = getFragmentManager();//                .commit();

//
//        fm.beginTransaction()
//                .replace(R.id.tab3, masFragment)
//                .commit();
//    }

    public void showNoticaDetail(Notica notica) {

        noticaDetailFragment.notica = notica;
        FragmentManager fm = getFragmentManager();

        fm.beginTransaction()
                .replace(R.id.tab1, noticaDetailFragment)
                .addToBackStack(null)
                .commit();
    }

    public void showEnvivoDetail(Envivo envivo) {

        envivoDetailFragment.envivo = envivo;
        FragmentManager fm = getFragmentManager();

        fm.beginTransaction()
                .replace(R.id.tab2, envivoDetailFragment)
                .addToBackStack(null)
                .commit();
    }

    public void showMasDetail(Mas mas) {

        masDetailFragment.mas = mas;
//        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(mas.url));
//        startActivity(browserIntent);
        FragmentManager fm = getFragmentManager();

        fm.beginTransaction()
                .replace(R.id.tab3, masDetailFragment)
                .addToBackStack(null)
                .commit();
    }

    public void playNoticaAudio(String audioUrl) {
        try {
            isPlayNotica = true;

            noticaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            noticaPlayer.setDataSource(audioUrl);

            Log.e("notica audio status: ", "play");
            Log.e("notica audio url: ", audioUrl);

            noticaPlayer.prepareAsync();

            noticaDetailFragment.vpAudio.setPagingEnabled(false);
            noticaPlayer.setOnPreparedListener(new FFmpegMediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(FFmpegMediaPlayer mp) {
                    Log.e("notica audio status: ", "start");
                    mp.setVolume(1, 1);
                    mp.start();
                }
            });
            noticaPlayer.setOnCompletionListener(new FFmpegMediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(FFmpegMediaPlayer mp) {
                    stopNoticaAudio();
                }
            });
            if (audioPageFragment != null) {
                audioPageFragment.imageView.setImageResource(R.drawable.audio_stop);
            }
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    public void stopNoticaAudio() {

        if (noticaDetailFragment.vpAudio != null) {
            noticaDetailFragment.vpAudio.setPagingEnabled(true);
        }
        if (audioPageFragment != null) {
            audioPageFragment.imageView.setImageResource(R.drawable.audio_play);
        }

        Log.e("notica audio status: ", "stop");

        if (noticaPlayer != null) {
            noticaPlayer.stop();
            noticaPlayer.reset();
        }

        audioPageFragment = null;
        isPlayNotica = false;
    }
}