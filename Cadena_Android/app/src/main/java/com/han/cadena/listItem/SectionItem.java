package com.han.cadena.listItem;

import com.han.cadena.R;

public class SectionItem extends MyListItem {
	public String title;
	public SectionItem(String _title) {
		title = _title;
		layoutID = R.layout.row_section;
	}
}