package com.han.cadena.listItem;

/**
 * Created by COREI3 on 2/13/2015.
 */
public class ImageItem {

    public String imagePath;
    public String text;

    public ImageItem(String _imagePath, String _text) {
        imagePath = _imagePath;
        text = _text;
    }
}