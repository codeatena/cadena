package com.han.cadena.listItem;

/**
 * Created by COREI3 on 2/22/2015.
 */
public class VideoItem extends ImageItem {
    public String videoPath;

    public VideoItem(String _videoPath, String _imagePath, String _text) {
        super(_imagePath, _text);
        videoPath = _videoPath;
    }
}
