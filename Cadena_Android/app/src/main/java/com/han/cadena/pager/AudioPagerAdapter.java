package com.han.cadena.pager;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.han.cadena.listItem.AudioItem;

import java.util.ArrayList;

/**
 * Created by COREI3 on 2/13/2015.
 */
public class AudioPagerAdapter extends FragmentPagerAdapter {
    ArrayList<AudioItem> arrAudioItems = new ArrayList<AudioItem>();

    public AudioPagerAdapter(FragmentManager fm, ArrayList<AudioItem> _arrAudioItems) {
        super(fm);
        arrAudioItems = _arrAudioItems;
    }

    @Override
    public Fragment getItem(int index) {

        AudioItem audioItem = arrAudioItems.get(index);
        String url = audioItem.audioPath;
        String text = audioItem.text;

        AudioPageFragment audioPageFragment = new AudioPageFragment();
        audioPageFragment.audioItem = audioItem;

        return audioPageFragment;
    }

    @Override
    public int getCount() {
        // get item count - equal to number of tabs
        return arrAudioItems.size();
    }
}
