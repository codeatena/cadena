package com.han.cadena.model;

/**
 * Created by COREI3 on 2/11/2015.
 */
public class Envivo {

    public String url;
    public String title;
    public String desc;
    public String audioUrl;

    public String imageUrl;
    public String title1;
    public String desc1;

    public int imageResouceID;

    public AudioStream audioStream;

    public Envivo(String _title, String _desc, String _url, String _audioUrl, int _imageResouceID) {
        title = _title;
        desc = _desc;
        url = _url;
        audioUrl = _audioUrl;
        imageResouceID = _imageResouceID;
        audioStream = new AudioStream(url);
    }
}