package com.han.cadena.listItem;

import com.han.cadena.R;
import com.han.cadena.model.Mas;

/**
 * Created by COREI3 on 2/11/2015.
 */
public class MasItem extends MyListItem {

    public Mas mas;

    public MasItem(Mas _mas) {
        mas = _mas;
        layoutID = R.layout.row_general_cell;
    }
}
