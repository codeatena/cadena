package com.han.cadena.model;

import android.util.Log;

import com.han.cadena.TabbedFragment;

/**
 * Created by COREI3 on 3/3/2015.
 */
public class AudioStream {

    public static final int LOADING = 1;
    public static final int PLAYING = 2;
    public static final int PAUSE = 3;

    public int nStatus;
    public String url;

    public AudioStream(String _url) {
        url = _url;
        nStatus = LOADING;
    }

    public static void pause(String url) {
        for (Envivo envivo: TabbedFragment.getInstance().arrEnvivos) {
            if (envivo.audioUrl.equals(url)) {
                envivo.audioStream.nStatus = PAUSE;
                break;
            }
        }
        if (url.equals(TabbedFragment.CADENA_AUDIO_URL)) {
            TabbedFragment.getInstance().playerCadena.setVolume(0, 0);
        }
        if (url.equals(TabbedFragment.CORDOBA_AUDIO_URL)) {
            TabbedFragment.getInstance().playerCordova.setVolume(0, 0);
        }
        if (url.equals(TabbedFragment.RADIO_AUDIO_URL)) {
            TabbedFragment.getInstance().playerRadio.setVolume(0, 0);
        }
        if (url.equals(TabbedFragment.EXTRA__AUDIO_URL)) {
            TabbedFragment.getInstance().playerExtra.setVolume(0, 0);
        }
    }

    public static void allPause() {
        for (Envivo envivo: TabbedFragment.getInstance().arrEnvivos) {
            envivo.audioStream.nStatus = PAUSE;
        }
        TabbedFragment.getInstance().playerCadena.setVolume(0, 0);
        TabbedFragment.getInstance().playerCordova.setVolume(0, 0);
        TabbedFragment.getInstance().playerRadio.setVolume(0, 0);
        TabbedFragment.getInstance().playerExtra.setVolume(0, 0);
    }

    public static void play(String url, float val) {
        Log.e("volume: ", Float.toString(val));
        for (Envivo envivo: TabbedFragment.getInstance().arrEnvivos) {
            if (envivo.audioUrl.equals(url)) {
                envivo.audioStream.nStatus = PLAYING;
                break;
            }
        }
        if (url.equals(TabbedFragment.CADENA_AUDIO_URL)) {
            TabbedFragment.getInstance().playerCadena.setVolume(val, val);
        }
        if (url.equals(TabbedFragment.CORDOBA_AUDIO_URL)) {
            TabbedFragment.getInstance().playerCordova.setVolume(val, val);
        }
        if (url.equals(TabbedFragment.RADIO_AUDIO_URL)) {
            TabbedFragment.getInstance().playerRadio.setVolume(val, val);
        }
        if (url.equals(TabbedFragment.EXTRA__AUDIO_URL)) {
            TabbedFragment.getInstance().playerExtra.setVolume(val, val);
        }
    }

    public static void setVolume(String url, float val) {
        if (url.equals(TabbedFragment.CADENA_AUDIO_URL)) {
            TabbedFragment.getInstance().playerCadena.setVolume(val, val);
        }
        if (url.equals(TabbedFragment.CORDOBA_AUDIO_URL)) {
            TabbedFragment.getInstance().playerCordova.setVolume(val, val);
        }
        if (url.equals(TabbedFragment.RADIO_AUDIO_URL)) {
            TabbedFragment.getInstance().playerRadio.setVolume(val, val);
        }
        if (url.equals(TabbedFragment.EXTRA__AUDIO_URL)) {
            TabbedFragment.getInstance().playerExtra.setVolume(val, val);
        }
    }
}