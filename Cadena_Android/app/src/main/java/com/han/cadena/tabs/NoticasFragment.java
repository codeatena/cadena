package com.han.cadena.tabs;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.Xml;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.han.cadena.R;
import com.han.cadena.TabbedFragment;
import com.han.cadena.listItem.NoticaItem;
import com.han.cadena.model.Notica;
import com.han.utility.DialogUtility;
import com.han.cadena.listItem.MyListItem;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.xmlpull.v1.XmlPullParser;

import java.io.StringReader;
import java.util.ArrayList;

public class NoticasFragment extends Fragment {

    ListView listView;

    public static ArrayList<MyListItem> arrItem = new ArrayList<MyListItem>();

    MyListAdapter adpList;
    TextView txtCategory;

    ImageView imgMenu;
    ImageView imgRefresh;

    int curIndex = 0;

    public static final String NOTICAS_URL = "http://www.cadena3.com/app/noticias.asp";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_noticas, container, false);
        listView = (ListView) view.findViewById(R.id.listView);
        txtCategory = (TextView) view.findViewById(R.id.category_textView);

        imgMenu = (ImageView) view.findViewById(R.id.menu_imageView);
        imgRefresh = (ImageView) view.findViewById(R.id.refresh_imageView);

        initValue();
        initEvent();

        return view;
    }

    private void initValue() {

//        arrItem = new ArrayList<MyListItem>();
        adpList = new MyListAdapter(getActivity(), arrItem);
        listView.setAdapter(adpList);

        if (curIndex == 0) {
            txtCategory.setText("Portada");
        }
        if (curIndex == 10) {
            txtCategory.setText("Política y Economía");
        }
        if (curIndex == 6) {
            txtCategory.setText("Sociedad");
        }
        if (curIndex == 15) {
            txtCategory.setText("Internacionales");
        }
        if (curIndex == 12) {
            txtCategory.setText("Deportes");
        }
        if (curIndex == 9) {
            txtCategory.setText("Espectáculos");
        }
        if (curIndex == 11) {
            txtCategory.setText("Tecnología");
        }
        if (curIndex == 14) {
            txtCategory.setText("Agro");
        }
    }

    public void drawListView(ArrayList<Notica> arrNoticas) {

        arrItem.clear();

        for (int i = 0; i < arrNoticas.size(); i++) {
            Notica notica = arrNoticas.get(i);
            NoticaItem noticaItem = null;
            if (i == 0) {
                noticaItem = new NoticaItem(notica, NoticaItem.ITEM_ONLY_IMAGE);
            } else {
                noticaItem = new NoticaItem(notica, NoticaItem.ITEM_GENERAL);
            }
            arrItem.add(noticaItem);
        }

        adpList.notifyDataSetChanged();
    }

    private void initEvent() {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                NoticaItem noticaItem = (NoticaItem) arrItem.get(position);
                TabbedFragment.getInstance().showNoticaDetail(noticaItem.notica);
            }
        });
        imgMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCategoryDialog();
            }
        });
        imgRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refreshNoticas(curIndex);
            }
        });
    }

    private ArrayList<Notica> parseNoticas(String strResponse) {
        ArrayList<Notica> arrNoticas = new ArrayList<Notica>();

        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setInput(new StringReader(strResponse));

            int eventType = parser.getEventType();
            String name = null;
            String strText = null;
            Notica notica = null;

            while (eventType != XmlPullParser.END_DOCUMENT) {
                switch (eventType) {
                    case XmlPullParser.START_TAG:
                        name = parser.getName();
                        if (name.equals("node")) {
                            notica = new Notica();
                        } else if (notica != null) {
                            if (name.equals("title")){
                                notica.title = parser.nextText();
                            } else if (name.equals("bajada")) {
                                notica.bajada = parser.nextText();
                            } else if (name.equals("id")) {
                                notica.id = parser.nextText();
                            } else if (name.equals("img")) {
                                notica.imgUrl = parser.nextText();
                            } else if (name.equals("date")) {
                                notica.date = parser.nextText();
                            }
                        }
                        break;
                    case XmlPullParser.TEXT:
                        strText = parser.getText();
                        break;
                    case XmlPullParser.END_TAG:
                        name = parser.getName();
                        if (name.equalsIgnoreCase("node") && notica != null){
                            arrNoticas.add(notica);
                        }
                        break;
                    default:
                        break;
                }
                eventType = parser.next();
            }
            Log.e("Record count: ", Integer.toString(arrNoticas.size()));
        } catch (Exception e){
            e.printStackTrace();
        }

        return arrNoticas;
    }

    private void showCategoryDialog() {
        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        txtCategory.setText("Portada");
                        refreshNoticas(0);
                        break;
                    case 1:
                        txtCategory.setText("Política y Economía");
                        refreshNoticas(10);
                        break;
                    case 2:
                        txtCategory.setText("Sociedad");
                        refreshNoticas(6);
                        break;
                    case 3:
                        txtCategory.setText("Internacionales");
                        refreshNoticas(15);
                        break;
                    case 4:
                        txtCategory.setText("Deportes");
                        refreshNoticas(12);
                        break;
                    case 5:
                        txtCategory.setText("Espectáculos");
                        refreshNoticas(9);
                        break;
                    case 6:
                        txtCategory.setText("Tecnología");
                        refreshNoticas(11);
                        break;
                    case 7:
                        txtCategory.setText("Agro");
                        refreshNoticas(14);
                        break;
                    case 8:
                        dialog.dismiss();
                        break;
                }
            }
        };

        new AlertDialog.Builder(getActivity())
                .setItems(R.array.notica_categories, listener)
                .setCancelable(true)
                .show();
    }

    private void refreshNoticas(int index) {
        curIndex = index;
        String url = NOTICAS_URL + "?id=" + Integer.toString(curIndex);

        final ProgressDialog progressDialog = DialogUtility.getProgressDialog(getActivity());
        progressDialog.show();

        Ion.with(getActivity())
                .load(url)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String strResponse) {
                        progressDialog.dismiss();
                        // chirp chirp
                        if (e != null)
                            e.printStackTrace();
                        else {
                            Log.e("noticas response: ", strResponse);
                            drawListView(parseNoticas(strResponse));
                        }
                    }
                });
    }
}
