package com.han.cadena.listItem;

import com.han.cadena.R;
import com.han.cadena.model.Envivo;

/**
 * Created by COREI3 on 2/11/2015.
 */
public class EnvivoItem extends MyListItem {
    public Envivo envivo;

    public EnvivoItem(Envivo _envivo) {
        envivo = _envivo;
        layoutID = R.layout.row_general_cell;
    }
}
