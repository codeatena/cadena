package com.han.cadena.detail;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.Xml;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.han.cadena.R;
import com.han.cadena.TabbedFragment;
import com.han.cadena.model.Envivo;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.xmlpull.v1.XmlPullParser;

import java.io.StringReader;

/**
 * Created by COREI3 on 2/11/2015.
 */
public class EnvivoDetailFragment extends Fragment {

    LinearLayout llytBack;
    TextView txtBack;

    final int MAX_VOLUME = 100;

    ImageView imageView;
    TextView txtTitle;
    TextView txtDesc;
    TextView txtState;
    Button btnAudio;
    SeekBar seekBar;

//    AudioStream audioStream;
    public String audioUrl = "";

    public Envivo envivo;

    public static EnvivoDetailFragment ins;

    public static EnvivoDetailFragment getInstance() {
        return ins;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_envivo_detail, container, false);

        llytBack = (LinearLayout) view.findViewById(R.id.back_linearLayout);
        txtBack = (TextView) view.findViewById(R.id.back_textView);

        imageView = (ImageView) view.findViewById(R.id.imageView);
        txtTitle = (TextView) view.findViewById(R.id.title_textView);
        txtDesc = (TextView) view.findViewById(R.id.desc_textView);
        txtState = (TextView) view.findViewById(R.id.state_textView);
        seekBar = (SeekBar) view.findViewById(R.id.seekBar);

        btnAudio = (Button) view.findViewById(R.id.audio_button);

        initValue();
        initEvent();

        ins = this;

        return view;
    }

    private void initValue() {
        txtTitle.setText("");
        txtDesc.setText("");
        txtBack.setText(envivo.title);
        seekBar.setProgress(MAX_VOLUME);

        if (!audioUrl.equals(envivo.audioUrl)) {
            stopAudio();
//            audioUrl = envivo.audioUrl;
//            audioStream = envivo.audioStream;
            playAudio();
        } else {
            if (TabbedFragment.mp != null) {
                if (TabbedFragment.mp.isPlaying()) {
                    txtState.setText("Estado: Reproduciendo");
                    btnAudio.setBackgroundResource(R.drawable.pause);
                } else {
                    txtState.setText("Estatdo: Almacenando");
                    btnAudio.setBackgroundResource(R.drawable.play);
                }
            } else {
                txtState.setText("Estatdo: Detenido");
                btnAudio.setBackgroundResource(R.drawable.play);
            }
        }

        Ion.with(getActivity())
                .load(envivo.url)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String strResponse) {
                        // chirp chirp
                        if (e != null)
                            e.printStackTrace();
                        else {
                            Log.e("envivo response: ", strResponse);
                            parseEnvivo(strResponse);
                        }
                    }
                });
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setRetainInstance(true);
    }

    private void stopAudio() {
        Log.e("audio state: ", audioUrl + " stop");
        txtState.setText("Estatdo: Detenido");
        btnAudio.setBackgroundResource(R.drawable.play);

        TabbedFragment.getInstance().stopEnvivoAudio();

//        if (audioStream != null) {
//            AudioStream.allPause();
//        }
    }

    private void playAudio() {

        Log.e("audio state: ", envivo.audioUrl + ": play");

        try {
            audioUrl = envivo.audioUrl;

//            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

            float val = (float) (Math.log(MAX_VOLUME - seekBar.getProgress()) / Math.log(MAX_VOLUME));

            txtState.setText("Estado: Almacenando");
            btnAudio.setBackgroundResource(R.drawable.play);

            TabbedFragment.getInstance().playEnvivoAudio(audioUrl);
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }

//        try {
//            if (audioStream.nStatus == AudioStream.LOADING) {
//                Log.e("audio state: ", "loading");
//
//                txtState.setText("Estado: Almacenando");
//                btnAudio.setBackgroundResource(R.drawable.play);
//            } else {
//                startPlay();
//            }
//        } catch (Exception e) {
//            // TODO: handle exception
//            e.printStackTrace();
//        }
    }

    public void startPlay() {

        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    txtState.setText("Estado: Reproduciendo");
                    btnAudio.setBackgroundResource(R.drawable.pause);
                }
            });
        }
        if (seekBar.getProgress() == MAX_VOLUME) {
//            AudioStream.play(audioUrl, 1);
            TabbedFragment.mp.setVolume(1.0f, 1.0f);
        } else {
            float val = (float) (Math.log(MAX_VOLUME - seekBar.getProgress()) / Math.log(MAX_VOLUME));
            TabbedFragment.mp.setVolume(1 - val, 1 - val);
//            AudioStream.play(audioUrl, 1 - val);
        }
    }

    private void initEvent() {
        btnAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (audioStream != null) {
//                    if (audioStream.nStatus == AudioStream.PLAYING) {
//                        stopAudio();
//                    } else if (audioStream.nStatus == AudioStream.PAUSE) {
//                        playAudio();
//                    }
//                }
                if (TabbedFragment.mp != null) {
                    if (TabbedFragment.mp.isPlaying()) {
                        stopAudio();
                    } else {
                        playAudio();
                    }
                } else {
                    playAudio();
                }
            }
        });
        llytBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar arg0) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar arg0) {

            }

            @Override
            public void onProgressChanged(SeekBar arg0, int arg1, boolean arg2) {

                if (TabbedFragment.mp != null) {

                    if (MAX_VOLUME == arg1) {
                        TabbedFragment.mp.setVolume(1.0f, 1.0f);
                    } else {
                        float val = (float) (Math.log(MAX_VOLUME - arg1) / Math.log(MAX_VOLUME));
                        TabbedFragment.mp.setVolume(1 - val, 1 - val);
                    }
                }
            }
        });
    }

    private void parseEnvivo(String strResponse) {
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setInput(new StringReader(strResponse));

            int eventType = parser.getEventType();
            String name = null;


            String key = "";
            String str = "";

            while (eventType != XmlPullParser.END_DOCUMENT) {
                switch (eventType) {
                    case XmlPullParser.START_TAG:
                        name = parser.getName();
                        if (name.equals("key")) {
                            key = parser.nextText();
                        }
                        if (name.equals("string")) {
                            str = parser.nextText();
                            if (key.equals("title")) {
                                envivo.title1 = str;
                            }
                            if (key.equals("description")) {
                                envivo.desc1 = str;
                            }
                            if (key.equals("img")) {
                                envivo.imageUrl = str;
                            }
                        }
                        break;
                    case XmlPullParser.TEXT:
                        break;
                    case XmlPullParser.END_TAG:
                        break;
                    default:
                        break;
                }
                eventType = parser.next();
            }
            redraw();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private void redraw() {
        txtTitle.setText(envivo.title1);
        txtDesc.setText(envivo.desc1);
        Ion.with(imageView)
                .load(envivo.imageUrl);
    }
}
