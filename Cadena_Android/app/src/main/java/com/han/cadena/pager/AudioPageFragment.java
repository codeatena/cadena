package com.han.cadena.pager;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.han.cadena.R;
import com.han.cadena.TabbedFragment;
import com.han.cadena.listItem.AudioItem;

/**
 * Created by COREI3 on 2/13/2015.
 */
public class AudioPageFragment extends Fragment {

    public ImageView imageView;
    TextView textView;
    AudioItem audioItem;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_audio_page, container, false);

        imageView = (ImageView) view.findViewById(R.id.imageView);
        textView = (TextView) view.findViewById(R.id.textView);
        textView.setText(audioItem.text);

        initValue();
        initEvent();

        return view;
    }

    private void initValue() {

    }

    private void initEvent() {
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TabbedFragment.getInstance().isPlayNotica) {
                    TabbedFragment.getInstance().audioPageFragment = AudioPageFragment.this;
                    TabbedFragment.getInstance().stopNoticaAudio();
                } else {
                    TabbedFragment.getInstance().stopNoticaAudio();
                    TabbedFragment.getInstance().audioPageFragment = AudioPageFragment.this;
                    TabbedFragment.getInstance().playNoticaAudio(audioItem.audioPath);
                }
            }
        });
    }
}
