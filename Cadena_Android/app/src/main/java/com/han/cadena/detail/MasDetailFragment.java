package com.han.cadena.detail;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.han.cadena.R;
import com.han.cadena.model.Mas;
import com.han.utility.DefaultWebClient;

/**
 * Created by COREI3 on 2/11/2015.
 */
public class MasDetailFragment extends Fragment {

    LinearLayout llytBack;
    TextView txtBack;

    ImageView imgRefreshButton;

    public WebView webView;
    public Mas mas;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_mas_detail, container, false);

        llytBack = (LinearLayout) view.findViewById(R.id.back_linearLayout);
        txtBack = (TextView) view.findViewById(R.id.back_textView);
        imgRefreshButton = (ImageView) view.findViewById(R.id.refresh_imageView);

        webView = (WebView) view.findViewById(R.id.webView);

        initValue();
        initEvent();

        return view;
    }

    private void initValue() {

        txtBack.setText(mas.title);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setAllowFileAccess(true);
        webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setDatabaseEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);

        webView.setWebChromeClient(new WebChromeClient());
        webView.setWebViewClient(new DefaultWebClient(getActivity()));

        webView.loadUrl(mas.url);
    }

    private void initEvent() {
        llytBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });
        imgRefreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                webView.setWebViewClient(new DefaultWebClient(getActivity()));
                webView.loadUrl(mas.url);
            }
        });
    }
}