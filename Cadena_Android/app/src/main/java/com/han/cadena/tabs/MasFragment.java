package com.han.cadena.tabs;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.han.cadena.R;
import com.han.cadena.TabbedFragment;
import com.han.cadena.listItem.MasItem;
import com.han.cadena.listItem.MyListItem;
import com.han.cadena.model.Mas;

import java.util.ArrayList;

public class MasFragment extends Fragment {

    public static final String FOOTBALL_URL = "http://www.cadena3.com/app/futbolmobile.asp";

//    public static final String FOOTBALL_URL = "http://www.cadena3.com/futbol/tablas-futbol.asp";
    public static final String CINEMA_URL = "http://www.cadena3.com/app/cine_mobile.asp";

    ListView listView;
    ArrayList<MyListItem> arrItem;
    MyListAdapter adpList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_mas, container, false);
        listView = (ListView) view.findViewById(R.id.listView);

        initValue();
        initEvent();

        return view;
    }

    private void initValue() {
        arrItem = new ArrayList<MyListItem>();

        arrItem.add(new MasItem(new Mas("Fútbol", "Todos los partidos minuto a minuto", FOOTBALL_URL, R.drawable.futbol)));
        arrItem.add(new MasItem(new Mas("Cine", "Salas, días y horarios de todo el País", CINEMA_URL, R.drawable.cine)));

        adpList = new MyListAdapter(getActivity(), arrItem);
        listView.setAdapter(adpList);
    }

    private void initEvent() {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MasItem masItem = (MasItem) arrItem.get(position);
                TabbedFragment.getInstance().showMasDetail(masItem.mas);
            }
        });
    }
}
