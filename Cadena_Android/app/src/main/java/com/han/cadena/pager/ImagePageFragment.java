package com.han.cadena.pager;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.han.cadena.R;
import com.han.cadena.listItem.ImageItem;
import com.koushikdutta.ion.Ion;

/**
 * Created by COREI3 on 2/13/2015.
 */
public class ImagePageFragment extends Fragment {

    ImageView imageView;
    TextView textView;
    ImageItem imageItem;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragments_image_page, container, false);

        imageView = (ImageView) view.findViewById(R.id.imageView);
        textView = (TextView) view.findViewById(R.id.textView);

        Ion.with(imageView)
                .load(imageItem.imagePath);
        textView.setText(imageItem.text);

        return view;
    }
}
