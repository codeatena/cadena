package com.han.cadena.model;

import java.util.ArrayList;

/**
 * Created by COREI3 on 2/8/2015.
 */
public class Notica {
    public String id;
    public String title;
    public String bajada;
    public String date;
    public String imgUrl;

    public String category;
    public String copete;
    public String volanta;
    public String text;

    public ArrayList<String> arrImageUrls = new ArrayList<String>();
    public ArrayList<String> arrImageTexts = new ArrayList<String>();

    public ArrayList<String> arrAudioUrls = new ArrayList<String>();
    public ArrayList<String> arrAudioTexts = new ArrayList<String>();
    public String url;
}