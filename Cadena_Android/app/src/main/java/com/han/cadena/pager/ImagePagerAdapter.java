package com.han.cadena.pager;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.han.cadena.listItem.ImageItem;
import com.han.cadena.listItem.VideoItem;

import java.util.ArrayList;

/**
 * Created by COREI3 on 2/13/2015.
 */
public class ImagePagerAdapter extends FragmentPagerAdapter {

    ArrayList<ImageItem> arrImageItems = new ArrayList<ImageItem>();

    public ImagePagerAdapter(FragmentManager fm, ArrayList<ImageItem> _arrImageItems) {
        super(fm);
        arrImageItems = _arrImageItems;
    }

    @Override
    public Fragment getItem(int index) {

        ImageItem imageItem = arrImageItems.get(index);

        if (imageItem instanceof VideoItem) {

            VideoPageFragment videoPageFragment = new VideoPageFragment();
            videoPageFragment.videoItem = (VideoItem) imageItem;

            return videoPageFragment;
        }

        ImagePageFragment imagePageFragment = new ImagePageFragment();
        imagePageFragment.imageItem = imageItem;

        return imagePageFragment;
    }

    @Override
    public int getCount() {
        // get item count - equal to number of tabs
        return arrImageItems.size();
    }
}