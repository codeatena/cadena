package com.han.cadena;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Xml;

import com.han.cadena.listItem.NoticaItem;
import com.han.cadena.model.Notica;
import com.han.cadena.tabs.NoticasFragment;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.xmlpull.v1.XmlPullParser;

import java.io.StringReader;
import java.util.ArrayList;


public class SplashActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        initValue();
        initEvent();
    }

    private void initValue() {
        String url = NoticasFragment.NOTICAS_URL + "?id=0";
        Ion.with(this)
                .load(url)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String strResponse) {
                        // chirp chirp
                        if (e != null)
                            e.printStackTrace();
                        else {
                            Log.e("noticas potado response: ", strResponse);
                            ArrayList<Notica> arrNoticas = parseNoticas(strResponse);

                            NoticasFragment.arrItem.clear();

                            for (int i = 0; i < arrNoticas.size(); i++) {
                                Notica notica = arrNoticas.get(i);
                                NoticaItem noticaItem = null;
                                if (i == 0) {
                                    noticaItem = new NoticaItem(notica, NoticaItem.ITEM_ONLY_IMAGE);
                                } else {
                                    noticaItem = new NoticaItem(notica, NoticaItem.ITEM_GENERAL);
                                }
                                NoticasFragment.arrItem.add(noticaItem);
                            }
                        }

                        startActivity(new Intent(SplashActivity.this, MainActivity.class));
                        finish();
                    }
                });
    }

    private ArrayList<Notica> parseNoticas(String strResponse) {
        ArrayList<Notica> arrNoticas = new ArrayList<Notica>();

        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setInput(new StringReader(strResponse));

            int eventType = parser.getEventType();
            String name = null;
            String strText = null;
            Notica notica = null;

            while (eventType != XmlPullParser.END_DOCUMENT) {
                switch (eventType) {
                    case XmlPullParser.START_TAG:
                        name = parser.getName();
                        if (name.equals("node")) {
                            notica = new Notica();
                        } else if (notica != null) {
                            if (name.equals("title")){
                                notica.title = parser.nextText();
                            } else if (name.equals("bajada")) {
                                notica.bajada = parser.nextText();
                            } else if (name.equals("id")) {
                                notica.id = parser.nextText();
                            } else if (name.equals("img")) {
                                notica.imgUrl = parser.nextText();
                            } else if (name.equals("date")) {
                                notica.date = parser.nextText();
                            }
                        }
                        break;
                    case XmlPullParser.TEXT:
                        strText = parser.getText();
                        break;
                    case XmlPullParser.END_TAG:
                        name = parser.getName();
                        if (name.equalsIgnoreCase("node") && notica != null){
                            arrNoticas.add(notica);
                        }
                        break;
                    default:
                        break;
                }
                eventType = parser.next();
            }
            Log.e("Record count: ", Integer.toString(arrNoticas.size()));
        } catch (Exception e){
            e.printStackTrace();
        }

        return arrNoticas;
    }

    private void initEvent() {

    }
}
