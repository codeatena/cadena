package com.han.cadena.pager;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.han.cadena.R;
import com.han.cadena.play.VideoActivity;
import com.han.cadena.listItem.VideoItem;
import com.koushikdutta.ion.Ion;

/**
 * Created by COREI3 on 2/22/2015.
 */
public class VideoPageFragment extends Fragment {

    ImageView imageView;
    TextView textView;
    Button btnPlay;

    VideoItem videoItem;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragments_video_page, container, false);

        imageView = (ImageView) view.findViewById(R.id.imageView);
        textView = (TextView) view.findViewById(R.id.textView);
        btnPlay = (Button) view.findViewById(R.id.play_video_button);

        Ion.with(imageView)
                .load(videoItem.imagePath);
        textView.setText(videoItem.text);

        initValue();
        initEvent();

        return view;
    }

    private void initValue() {

    }

    private void initEvent() {
        btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VideoActivity.videoPath = videoItem.videoPath;
                startActivity(new Intent(getActivity(), VideoActivity.class));
            }
        });
    }
}
