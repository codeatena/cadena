package com.han.cadena.tabs;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.han.cadena.R;
import com.han.cadena.TabbedFragment;
import com.han.cadena.listItem.EnvivoItem;
import com.han.cadena.listItem.MyListItem;
import com.han.cadena.model.Envivo;

import java.util.ArrayList;

public class EnvivoFragment extends Fragment {



    public ListView listView;
    MyListAdapter adpList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_envivo, container, false);
        listView = (ListView) view.findViewById(R.id.listView);

        initValue();
        initEvent();

        return view;
    }

    private void initValue() {
        ArrayList<MyListItem> arrItems = new ArrayList<MyListItem>();

        for (Envivo envivo: TabbedFragment.getInstance().arrEnvivos) {
            arrItems.add(new EnvivoItem(envivo));
        }

        adpList = new MyListAdapter(getActivity(), arrItems);
        listView.setAdapter(adpList);
    }

    @Override
    public void onStart() {
        super.onStart();
        initValue();
    }

    private void initEvent() {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Envivo envivo = (Envivo) TabbedFragment.getInstance().arrEnvivos.get(position);
                TabbedFragment.getInstance().showEnvivoDetail(envivo);
            }
        });
    }
}
