package com.han.cadena.detail;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.util.Xml;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.han.cadena.R;
import com.han.cadena.TabbedFragment;
import com.han.cadena.listItem.AudioItem;
import com.han.cadena.listItem.ImageItem;
import com.han.cadena.listItem.VideoItem;
import com.han.cadena.model.Notica;
import com.han.cadena.pager.AudioPagerAdapter;
import com.han.cadena.pager.ImagePagerAdapter;
import com.han.widget.ViewPager.MyViewPager;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.viewpagerindicator.CirclePageIndicator;

import org.xmlpull.v1.XmlPullParser;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;

public class NoticaDetailFragment extends Fragment {

    static NoticaDetailFragment instance = null;

    LinearLayout llytBack;

    ImageView imgSmallFontButton;
    ImageView imgLargeFontButton;
    ImageView imgUploadButton;

    public Notica notica;
    public HashMap<String, String> hashMap;

    final String NOTICA_URL = "http://www.cadena3.com/app/noticia.asp";

    TextView txtTitle;
    TextView txtCopete;
    TextView txtText;

    public ViewPager vpImage;
    public MyViewPager vpAudio;

    CirclePageIndicator imageIndicator;
    CirclePageIndicator audioIndicator;

    ImagePagerAdapter imagePagerAdapter;

    public NoticaDetailFragment() {

    }

    public static NoticaDetailFragment getInstance() {
        return instance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_notica_detail, container, false);

        llytBack = (LinearLayout) view.findViewById(R.id.back_linearLayout);

        imgSmallFontButton = (ImageView) view.findViewById(R.id.small_font_imageView);
        imgLargeFontButton = (ImageView) view.findViewById(R.id.large_font_imageView);
        imgUploadButton = (ImageView) view.findViewById(R.id.refresh_imageView);

        txtTitle = (TextView) view.findViewById(R.id.title_textView);
        txtCopete = (TextView) view.findViewById(R.id.copete_textView);
        txtText = (TextView) view.findViewById(R.id.text_textView);

        vpImage = (ViewPager) view.findViewById(R.id.image_viewPager);
        vpAudio = (MyViewPager) view.findViewById(R.id.audio_viewPager);

        imageIndicator = (CirclePageIndicator) view.findViewById(R.id.image_indicator);
        audioIndicator = (CirclePageIndicator) view.findViewById(R.id.audio_indicator);

        final float density = getResources().getDisplayMetrics().density;
//        imageIndicator.setBackgroundColor(0xFFCCCCCC);
        imageIndicator.setRadius(5 * density);
        imageIndicator.setPageColor(0xFF888888);
        imageIndicator.setFillColor(0xFFFFFFFF);
        imageIndicator.setStrokeColor(0x00000000);

//        audioIndicator.setBackgroundColor(0xFFCCCCCC);
        audioIndicator.setRadius(5 * density);
        audioIndicator.setPageColor(0xFFAAAAAA);
        audioIndicator.setFillColor(0xFF888888);
        audioIndicator.setStrokeColor(0x00000000);

        initValue();
        initEvent();
        setLargeFont();

        instance = this;

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void initValue() {

        String url = NOTICA_URL + "?id=" + notica.id;
//        String url = "http://www.cadena3.com/app/noticia.asp?id=140962";

        Log.e("notica detail: ", url);

        Ion.with(getActivity())
                .load(url)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String strResponse) {
                        // chirp chirp
                        if (e != null)
                            e.printStackTrace();
                        else {
                            Log.e("noticas response: ", strResponse);
                            parseNotica(strResponse);
                        }
                    }
                });
    }

    private void initEvent() {
        llytBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TabbedFragment.getInstance().stopNoticaAudio();
                getFragmentManager().popBackStack();
            }
        });
        imgSmallFontButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSmallFont();
            }
        });
        imgLargeFontButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setLargeFont();
            }
        });
        imgUploadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showShareDialog();
            }
        });
    }

    private void showShareDialog() {

        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, notica.title);
        sendIntent.putExtra(Intent.EXTRA_TITLE, notica.title);
        sendIntent.putExtra(Intent.EXTRA_TEXT, hashMap.get("url") + "\n\n\nSent from my Android");
        sendIntent.setType("text/plain");
        Intent chooser = Intent.createChooser(sendIntent, "Share Using...");
        startActivity(chooser);

//        startActivity(sendIntent, 1000);
    }

    private void setLargeFont() {
        txtTitle.setTextSize(20);
        txtCopete.setTextSize(15);
        txtText.setTextSize(15);
    }

    private void setSmallFont() {
        txtTitle.setTextSize(18);
        txtCopete.setTextSize(13);
        txtText.setTextSize(13);
    }

    private void parseNotica(String strResponse) {
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setInput(new StringReader(strResponse));

            int eventType = parser.getEventType();
            String name = null;

            String key = "";
            String str = "";

            hashMap = new HashMap<String, String>();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                switch (eventType) {
                    case XmlPullParser.START_TAG:
                        name = parser.getName();
                        if (name.equals("key")) {
                            key = parser.nextText();
                        }
                        if (name.equals("string")) {
                            str = parser.nextText();
                            Log.e("key, string: ", key + ", " + str);
                            hashMap.put(key, str);
                        }
                        break;
                    case XmlPullParser.TEXT:
                        break;
                    case XmlPullParser.END_TAG:
                        break;
                    default:
                        break;
                }
                eventType = parser.next();
            }

            redraw();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private void redraw() {

        for (String key: hashMap.keySet()) {
            if (key.equals("title")) {
                txtTitle.setText(hashMap.get(key));
            }
            if (key.equals("copete")) {
                txtCopete.setText(hashMap.get(key));
            }
            if (key.equals("text")) {
                txtText.setText(hashMap.get(key));
            }
        }

        ArrayList<ImageItem> arrImageItems = getImageItems();
        ArrayList<AudioItem> arrAudioItems = getAudioItems();
        ArrayList<VideoItem> arrVideoItems = getVideoItems();

        arrImageItems.addAll(0, arrVideoItems);

        ImagePagerAdapter imagePagerAdapter = new ImagePagerAdapter(getChildFragmentManager(), arrImageItems);
        vpImage.setAdapter(imagePagerAdapter);

        AudioPagerAdapter audioPagerAdapter = new AudioPagerAdapter(getChildFragmentManager(), arrAudioItems);
        vpAudio.setAdapter(audioPagerAdapter);

        audioIndicator.setViewPager(vpAudio);
        imageIndicator.setViewPager(vpImage);

        if (arrImageItems.size() == 0) {
            vpImage.setVisibility(View.GONE);
            imageIndicator.setVisibility(View.GONE);
        } else if (arrImageItems.size() == 1) {
            vpImage.setVisibility(View.VISIBLE);
            imageIndicator.setVisibility(View.GONE);
        } else {
            vpImage.setVisibility(View.VISIBLE);
            imageIndicator.setVisibility(View.VISIBLE);
        }

        if (arrAudioItems.size() == 0) {
            vpAudio.setVisibility(View.GONE);
            audioIndicator.setVisibility(View.GONE);
        } else if (arrAudioItems.size() == 1) {
            vpAudio.setVisibility(View.VISIBLE);
            audioIndicator.setVisibility(View.GONE);
        } else {
            vpAudio.setVisibility(View.VISIBLE);
            audioIndicator.setVisibility(View.VISIBLE);
        }
    }

    private ArrayList<ImageItem> getImageItems() {

        ArrayList<ImageItem> arrItems = new ArrayList<ImageItem>();

        for (int i = 1; i <= 10; i++) {
            String url = "";
            String text = "";
            for (String key: hashMap.keySet()) {
                if (key.equals("img" + Integer.toString(i))) {
                    url = hashMap.get(key);
                }
                if (key.equals("img" + Integer.toString(i) + "Text")) {
                    text = hashMap.get(key);
                }
            }
            if (url.equals("")) continue;
            arrItems.add(new ImageItem(url, text));
        }

        Log.e("image count: ", Integer.toString(arrItems.size()));
        return arrItems;
    }

    private ArrayList<VideoItem> getVideoItems() {

        ArrayList<VideoItem> arrItems = new ArrayList<VideoItem>();

        for (int i = 1; i <= 10; i++) {
            String videoPath = "";
            String imagePath = "";
            String text = "";
            for (String key: hashMap.keySet()) {
                if (key.equals("video" + Integer.toString(i))) {
                    videoPath = hashMap.get(key);
                }
                if (key.equals("video" + Integer.toString(i) + "Text")) {
                    text = hashMap.get(key);
                }
                if (key.equals("video" + Integer.toString(i) + "th")) {
                    imagePath = hashMap.get(key);
                }
            }
            if (videoPath.equals("")) continue;
            arrItems.add(new VideoItem(videoPath, imagePath, text));
        }

        Log.e("video count: ", Integer.toString(arrItems.size()));

        return arrItems;
    }

    private ArrayList<AudioItem> getAudioItems() {

        ArrayList<AudioItem> arrItems = new ArrayList<AudioItem>();

        for (int i = 1; i <= 10; i++) {
            String url = "";
            String text = "";

            for (String key: hashMap.keySet()) {
                if (key.equals("audio" + Integer.toString(i))) {
                    url = hashMap.get(key);
                }
                if (key.equals("audio" + Integer.toString(i) + "Text")) {
                    text = hashMap.get(key);
                }
            }

            if (url.equals("")) continue;
            arrItems.add(new AudioItem(url, text));
        }

        Log.e("audio count: ", Integer.toString(arrItems.size()));
        return arrItems;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        TabbedFragment.getInstance().stopNoticaAudio();
    }
}
