package com.han.cadena.listItem;

import com.han.cadena.R;
import com.han.cadena.model.Notica;

/**
 * Created by COREI3 on 2/8/2015.
 */
public class NoticaItem extends MyListItem {
    public Notica notica = new Notica();
    public int nType;

    public static final int ITEM_ONLY_IMAGE = 0;
    public static final int ITEM_GENERAL = 1;

    public NoticaItem(Notica _notica, int _nType) {
        nType = _nType;
        if (nType == ITEM_GENERAL) {
            layoutID = R.layout.row_notica_general;
        }
        if (nType == ITEM_ONLY_IMAGE) {
            layoutID = R.layout.row_notica_large;
        }
        notica = _notica;
    }
}