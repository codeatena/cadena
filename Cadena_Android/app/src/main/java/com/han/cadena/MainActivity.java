package com.han.cadena;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().hide();

        initWidget();
        initValue();
        initEvent();
    }

    private void initWidget() {
        TabbedFragment tabbedFragment = new TabbedFragment();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.main_layout, tabbedFragment).commit();
    }

    private void initValue() {

    }

    private void initEvent() {

    }
}
