package com.han.cadena.tabs;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.han.cadena.R;
import com.han.cadena.listItem.EnvivoItem;
import com.han.cadena.listItem.MasItem;
import com.han.cadena.listItem.NoticaItem;
import com.han.cadena.model.Envivo;
import com.han.cadena.model.Mas;
import com.han.cadena.model.Notica;
import com.han.cadena.listItem.MyListItem;
import com.han.cadena.listItem.SectionItem;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;

public class MyListAdapter extends ArrayAdapter<MyListItem> {

    public MyListAdapter(Context context, ArrayList<MyListItem> listItems) {
        super(context, 0, listItems);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        LayoutInflater inflater = LayoutInflater.from(getContext());

        MyListItem item = getItem(position);
        if (item instanceof NoticaItem) {
            NoticaItem noticaItem = (NoticaItem) item;
            Notica notica = noticaItem.notica;

            if (noticaItem.nType == NoticaItem.ITEM_ONLY_IMAGE) {
                view = inflater.inflate(noticaItem.layoutID, parent, false);

                TextView textView = (TextView) view.findViewById(R.id.textView);
                ImageView imageView = (ImageView) view.findViewById(R.id.imageView);

                Ion.with(imageView)
                        .load(notica.imgUrl);

                textView.setText(notica.title);
            }
            if (noticaItem.nType == NoticaItem.ITEM_GENERAL) {
                view = inflater.inflate(noticaItem.layoutID, parent, false);

                TextView textView= (TextView) view.findViewById(R.id.textView);
                ImageView imageView = (ImageView) view.findViewById(R.id.imageView);

                if (notica.imgUrl == null || notica.imgUrl.equals("")) {
                    imageView.setVisibility(View.GONE);
                    Ion.with(imageView)
                            .load(notica.imgUrl);
                } else {
                    imageView.setVisibility(View.VISIBLE);
                    Ion.with(imageView)
                            .load(notica.imgUrl);
                }

                textView.setText(notica.date + " | " + notica.title);
            }
        }
        if (item instanceof SectionItem) {
            SectionItem sectionItem = (SectionItem) item;
            view = inflater.inflate(sectionItem.layoutID, parent, false);

            TextView textView = (TextView) view.findViewById(R.id.title_textView);
            textView.setText(sectionItem.title);
        }
        if (item instanceof EnvivoItem) {
            EnvivoItem envivoItem = (EnvivoItem) item;

            view = inflater.inflate(envivoItem.layoutID, parent, false);

            TextView txtTitle = (TextView) view.findViewById(R.id.title_textView);
            TextView txtDesc = (TextView) view.findViewById(R.id.desc_textView);
            ImageView imageView = (ImageView) view.findViewById(R.id.imageView);

            Envivo envivo = envivoItem.envivo;

            txtTitle.setText(envivo.title);
            txtDesc.setText(envivo.desc);
            imageView.setImageResource(envivo.imageResouceID);
        }
        if (item instanceof MasItem) {
            MasItem masItem = (MasItem) item;

            view = inflater.inflate(masItem.layoutID, parent, false);

            TextView txtTitle = (TextView) view.findViewById(R.id.title_textView);
            TextView txtDesc = (TextView) view.findViewById(R.id.desc_textView);
            ImageView imageView = (ImageView) view.findViewById(R.id.imageView);

            Mas mas = masItem.mas;

            txtTitle.setText(mas.title);
            txtDesc.setText(mas.desc);
            imageView.setImageResource(mas.imageResouceID);
        }

        return view;
    }
}
