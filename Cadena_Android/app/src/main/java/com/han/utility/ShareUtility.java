package com.han.utility;

import android.content.Context;
import android.content.Intent;

/**
 * Created by COREI3 on 2/14/2015.
 */
public class ShareUtility {
    static ShareUtility instance;

    public static ShareUtility getInstance() {
        if (instance == null) {
            instance = new ShareUtility();
        }
        return instance;
    }

    public void shareEmailWithText(Context context, String title, String body) {
        Intent emailIntent = new Intent( android.content.Intent.ACTION_SEND);

        emailIntent.setType("plain/text");

        emailIntent.putExtra(Intent.EXTRA_EMAIL,
                new String[] { "abc@gmail.com" });

        emailIntent.putExtra(Intent.EXTRA_SUBJECT,
                title);

        emailIntent.putExtra(Intent.EXTRA_TEXT,
                body);

        context.startActivity(Intent.createChooser(
                emailIntent, "Send mail..."));
    }
}
