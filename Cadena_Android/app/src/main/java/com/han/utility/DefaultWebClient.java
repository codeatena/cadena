package com.han.utility;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.han.cadena.tabs.MasFragment;

public class DefaultWebClient extends WebViewClient {

    Context context;
    public ProgressDialog dlgLoading;

    public DefaultWebClient(Context _context) {
        context = _context;
    }

	@Override
	public void onPageStarted(WebView view, String url, Bitmap favicon) {
		// TODO Auto-generated method stub
        Log.e("webview url", url);

        super.onPageStarted(view, url, favicon);
        dlgLoading = DialogUtility.getProgressDialog(context);
        dlgLoading.show();
//        curUrl = url;
//        if (curUrl.equals(MasFragment.CINEMA_URL)) {
//            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
//            context.startActivity(browserIntent);
//        } else {
//
//        }
	}

	public boolean shouldOverrideUrlLoading(WebView webView, String url) {
		// TODO Auto-generated method stub

        Log.e("webview url: ", url);

        if (!url.contains(MasFragment.CINEMA_URL) && !url.contains(MasFragment.FOOTBALL_URL)) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            context.startActivity(browserIntent);

            return true;
        }

		return false;
	}

    @Override
	public void onPageFinished(WebView view, String url) {
        if (dlgLoading.isShowing()) {
            dlgLoading.dismiss();
        }
	}
	
	@Override
    public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {

        Log.e("WEB_VIEW_TEST", "error code:" + errorCode);

        if (dlgLoading.isShowing()) {
            dlgLoading.dismiss();
        }

        super.onReceivedError(view, errorCode, description, failingUrl);
    }
}